#include "Application.h"

Application::Application()
	: m_window(nullptr)
	, m_glContext(nullptr)
	, m_wWidth(1280)
	, m_wHeight(720)
{
}

Application::~Application()
{
}

bool Application::Init()
{
	//Init SDL
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER) < 0)
	{
		fprintf(stderr, "Failed to init SDL %s\n", SDL_GetError());
		return false;
	}

	//Init Window
	m_window = SDL_CreateWindow("VR App", 700, 100, m_wWidth, m_wHeight, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
	if (m_window == nullptr)
	{
		fprintf(stderr, "Failed to create window: %s\n", SDL_GetError());
		return false;
	}
	
	//Init GL
	m_glContext = SDL_GL_CreateContext(m_window);
	if (m_glContext == nullptr)
	{
		fprintf(stderr, "Failed to init OpenGL context: %s\n", SDL_GetError());
		return false;
	}

	glewExperimental = GL_TRUE;
	GLenum nGlewError = glewInit();
	if (nGlewError != GLEW_OK)
	{
		fprintf(stderr, "Warning: Glew init error: %s\n", glewGetErrorString(nGlewError));
		return false;
	}
	glGetError();

	if (SDL_GL_SetSwapInterval(0))
	{
		fprintf(stderr, "Warning: Unable to set VSync! SDL Error: %s\n", SDL_GetError());
		return false;
	}

	return true;
}

void Application::Shutdown()
{
	if (m_window)
	{
		SDL_DestroyWindow(m_window);
		m_window = nullptr;
	}

	SDL_Quit();
}

void Application::Run()
{

}

bool Application::HandleInput()
{
	return false;
}

void Application::RenderFrame()
{

}