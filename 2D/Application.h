#pragma once
#include <iostream>
#include <SDL/SDL.h>
#include <GL/glew.h>
#include <SDL/SDL_opengl.h>
#undef main

class Application
{
public:
	Application();
	~Application();

	bool Init();
	void Run();
	void Shutdown();

private:
	void RenderFrame();
	bool HandleInput();

private:
	uint32_t m_wWidth;
	uint32_t m_wHeight;
	SDL_Window* m_window;
	SDL_GLContext m_glContext;
};

